public class Pizza {
    private static short counter = 0;
    public final int number = nextCounter();
    String name;
    int quantity;
    String type;

    String[][] ingredients = {
            {"Tomato Paste", "1"},
            {"Cheese", "1"},
            {"Salami", "1.5"},
            {"Bacon", "1.2"},
            {"Garlic", "0.3"},
            {"Pepperoni", "0.7"},
            {"Corn", "0.6"},
            {"Olives", "0.5"}};
    public Pizza(String name, String type, int quantity){
        this.name = name;
        this.quantity = quantity;
        this.type = type;
    }

    private static int nextCounter() {
        return counter++;
    }

    public void setName(String name) {
        this.name = name;
    }

    void addIngredient(String ingredient){
        if (this.ingredients.length == 8){
            System.out.println("All ingredients have been added");
        } else {
            for (String[] s : ingredients) {
                if (s[0].equals(ingredient)) {
                    System.out.println("Please check your order");
                    break;
                }
            }
        }
    }
}
