import java.time.LocalTime;
import java.util.ArrayList;

public class Order {
    private static short counter = 10000;
    public final int number = nextCounter();
    int clientNumber;
    LocalTime orderTime = LocalTime.now();
    ArrayList<Pizza> pizzas;

    public Order(int clientNumber, ArrayList<Pizza> pizzas){
        this.clientNumber = clientNumber;
        this.pizzas = pizzas;
    }

    static Boolean isNotAllowablePizzaName(Pizza pizza){
        return  !(pizza.name.length() >= 4 && pizza.name.length() <= 20);
    }
    void checkPizzasNames(){
        int i=0;
        for (Pizza pizza : this.pizzas){
            if (isNotAllowablePizzaName(pizza)){
                pizza.setName(clientNumber + "_" + i);
            }
            i++;
        }
    }

    void printAttributesOfPizza(){
        for (Pizza pizza : this.pizzas){
            System.out.println(this.number + " : " + this.clientNumber + " : " + pizza.name + " : " + pizza.quantity);
        }
    }
    private static int nextCounter() {
        return counter++;
    }

    public String toString(){
        StringBuilder output = new StringBuilder();
        double totalCostOfOrder = 0;
        output.append("******************\nOrder: ").append(this.number).append("\nClient: ").append(this.clientNumber);
        for (Pizza pizza : this.pizzas) {
            output.append("\nName: ").append(pizza.name).append("\n----------------------\n").append(pizza.type);
            double totalCostOfPizza = 1;
            if (pizza.type.equals("Calzone")){
                totalCostOfPizza = 1.5;
            }
            output.append(": ").append(totalCostOfPizza).append(" €");

            for (int j = 0; j < pizza.ingredients.length; j++) {
                output.append("\n").append(pizza.ingredients[j][0]).append(": ").append(pizza.ingredients[j][1]).append(" €");
                totalCostOfPizza += Double.parseDouble(pizza.ingredients[j][1]);
            }
            totalCostOfPizza *= pizza.quantity;
            totalCostOfOrder += totalCostOfPizza;
            output.append("\n------------------\nTotal: ").append(totalCostOfPizza).append(" €\nQuantity: ").append(pizza.quantity);
            output.append("\n------------------------");
        }
        output.append("\nTotal sum: ").append(totalCostOfOrder).append(" $");
        output.append("\n*******************");
        return output.toString();
    }
}
