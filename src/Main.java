import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        Pizza pizza1 = new Pizza("Margarita", "Calzone", 2);
        Pizza pizza2 = new Pizza("Pepperoni", "Base", 3);
        ArrayList<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza1);
        pizzas.add(pizza2);
        Order order = new Order(7717, pizzas);
        order.checkPizzasNames();
        System.out.println(order.toString());

        Pizza pizza3 = new Pizza("Base ZZ", "Base", 12);
        ArrayList<Pizza> pizzas2 = new ArrayList<>();
        pizzas2.add(pizza3);
        Order order2 = new Order(4372, pizzas2);
        order2.checkPizzasNames();
        System.out.println(order2.toString());

    }
}
